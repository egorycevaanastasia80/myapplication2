package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.Comn.Comn;
import com.example.myapplication.wig.Paint;
import com.thebluealliance.spectrum.SpectrumPalette;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.UUID;

public class PaintAct extends AppCompatActivity implements SpectrumPalette.OnColorSelectedListener {
    Paint paint;
    private static final int REQUEST_PERMISSION = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint);

        SpectrumPalette spectrumPalette = findViewById(R.id.palette);
        spectrumPalette.setOnColorSelectedListener(this);

        paint = findViewById(R.id.paint);


    }
   @RequiresApi(api = Build.VERSION_CODES.M)
    public void Have(View view) {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    REQUEST_PERMISSION);
        }else {
            save();
        }

    }
    private void save() {
        Bitmap bitmap = paint.getBitmap();
        String file_name = UUID.randomUUID() + ".png";

        File folder = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES) +
                File.separator+getString(R.string.app_name));

        if (!folder.exists()) {
            folder.mkdirs();
        }
        try{

            FileOutputStream fileOutputStream = new FileOutputStream(folder+File.separator+file_name);
            bitmap.compress(Bitmap.CompressFormat.PNG,100,fileOutputStream);
            Toast.makeText(this, "Save", Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if(requestCode == REQUEST_PERMISSION && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED);
        save();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }



    @Override
    public void onColorSelected(int color) {
        Comn.COLOR_SELECTED = color;
    }
}

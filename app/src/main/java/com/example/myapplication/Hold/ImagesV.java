package com.example.myapplication.Hold;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Interface.ImageClick;
import com.example.myapplication.R;


public class ImagesV extends RecyclerView.ViewHolder {

    public ImageView imageV;

    private ImageClick imageClick;

    public void setImageClick(ImageClick imageClick) {
        this.imageClick = imageClick;
    }

    public ImagesV(@NonNull View itemView) {
        super(itemView);

        imageV = itemView.findViewById(R.id.image);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageClick.onClick(getAdapterPosition());
            }
        });
    }


}


package com.example.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Comn.Comn;
import com.example.myapplication.Hold.ImagesV;
import com.example.myapplication.Interface.ImageClick;
import com.example.myapplication.PaintAct;
import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class ImAdapter extends RecyclerView.Adapter<ImagesV> {
    private Context mContext;
    private List<Integer> listImages;

    public ImAdapter(Context mContext) {
        this.mContext = mContext;
        this.listImages = getImages();
    }

    private List<Integer> getImages() {
        List<Integer> results = new ArrayList<>();

        results.add(R.drawable.one);
        results.add(R.drawable.two);
        results.add(R.drawable.free);
        results.add(R.drawable.four);
        results.add(R.drawable.five);

        return results;
    }

    @NonNull
    @Override
    public ImagesV onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.image,parent, false);
        return new ImagesV(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesV holder, int position) {
        holder.imageV.setImageResource(listImages.get(position));

        holder.setImageClick(new ImageClick() {
            @Override
            public void onClick(int pos) {
                Comn.PICTURE_SELECTED = listImages.get(pos);
               mContext.startActivity(new Intent(mContext, PaintAct.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listImages.size();
    }
}
